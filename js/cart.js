var currentOrder = [];
var overallOrder = [];
var Id,Name,Price,Qty,Duration,Img;
var currentOrderTotal = 0;
// add a product to cart
function addToCart(id,name,price,qty,duration,img){
    var itemFound=false;
    currentOrder.forEach(item => {
        if(item.id==id && item.duration==duration){
            itemFound=true;
            if(!(qty==-1 && item.qty==1)){
                item.qty += qty;
            }
        }
    });

    if(!itemFound){
        currentOrder[currentOrder.length] = {
            "id" : id,
            "name" : name,
            "price" : price,
            "qty" : qty,
            "duration" : duration,
            "img" : img
        }
    }

    viewCOrder();
}

// view current order
function viewCOrder(){
    var txtOrder = "";
    currentOrderTotal = 0;
    currentOrder.forEach(item => {
        txtOrder+='<div class="product">\n'+
        '<div class="proLeft">\n'+
            '<img src="./img/icons/'+item.img+'" alt="">\n'+
        '</div>\n'+
        '<div class="proCenter">\n'+
            '<h3 style="text-align: center;">'+item.name+'</h3>\n'+
            '<p style="text-align: center;">Rs. '+item.price+' x '+item.qty+'</p>\n'+
            '<div class="relativeClass col12 footerIconCover">\n'+
                '<div class="btn" style="margin-right: 10px;" onclick="addToCart('+item.id+',\'\',0,-1,'+item.duration+',\'\')">\n'+
                    '<p>-</p>\n'+
                '</div>\n'+
                '<div class="btn" onclick="addToCart('+item.id+',\'\',0,1,'+item.duration+',\'\')">\n'+
                    '<p>+</p>\n'+
                '</div>\n'+
            '</div>\n'+
        '</div>\n'+
        '<div class="proRight">\n'+
            '<div class="btn" onclick="removePro('+item.id+','+item.duration+')">\n'+
                '<p>REMOVE</p>\n'+
            '</div>\n'+
            '<p style="text-align: center; margin-bottom: 10px;">'+item.duration+' h</p>\n'+
        '</div>\n'+
    '</div>';

        currentOrderTotal+=item.price*item.qty;

    });

    
    document.getElementById("currentOrder").innerHTML = txtOrder;
    document.getElementById("currentOrderTotal").innerHTML = "Total : Rs. "+currentOrderTotal;
}

function viewOVrder(){
    var txtOrder = "";
    currentOrderTotal = 0;
    overallOrder.forEach(item => {
        txtOrder+='<div class="product">\n'+
        '<div class="proLeft">\n'+
            '<img src="./img/icons/'+item.img+'" alt="">\n'+
        '</div>\n'+
        '<div class="proCenter">\n'+
            '<h3 style="text-align: center;">'+item.name+'</h3>\n'+
            '<p style="text-align: center;">Rs. '+item.price+' x '+item.qty+'</p>\n'+
        '</div>\n'+
        '<div class="proRight">\n'+
            '<p style="text-align: center; margin-bottom: 10px; margin-top: 20px;">'+item.duration+' h</p>\n'+
        '</div>\n'+
    '</div>';

        currentOrderTotal+=item.price*item.qty;

    });

    
    document.getElementById("overallOrder").innerHTML = txtOrder;
    document.getElementById("overallOrderTotal").innerHTML = "Total : Rs. "+currentOrderTotal;
}

function viewSelection(id,name,price,qty,duration,img){
    Id=id;
    Name=name;
    Price=price;
    Qty=qty;
    Duration=duration;
    Img=img;
    document.getElementById("duraion").selectedIndex = "0";
    document.getElementById("qty").value = "1";
    document.getElementById("model-name").innerHTML = name;
    document.getElementById("model-price").innerHTML = "Rs. "+ price;
    document.getElementById("model").style.display = "block";
}

function addCart(){
    switch(parseInt(document.getElementById("duraion").value)) {
        case 12:
            Price+=250;
        break;
        case 24:
            Price+=500;
        break;
        case 38:
            Price+=1000;
        break;
    }
    Duration=parseInt(document.getElementById("duraion").value);
    Qty=parseInt(document.getElementById("qty").value);

    addToCart(Id,Name,Price,Qty,Duration,Img);
    hideSelection();
}

function hideSelection(){
    document.getElementById("model").style.display = "none";
}

function removePro(id,duration){
    var position=0;
    currentOrder.forEach(item => {
        if(item.id==id && item.duration==duration){
            currentOrder.splice(position, 1);
            return;
        }
        position++;
    });
    viewCOrder();
}

function addToorderOverall(){
    if(currentOrder.length>0){
        overallOrder=currentOrder;
        currentOrder = [];
        viewCOrder();
        viewOVrder();
    }
}

function addtoFavo(){
    if(currentOrder.length>0){
        localStorage.setItem("fav",JSON.stringify(currentOrder));
        // var points=0;
        // if (!(localStorage.getItem("points") === null)) {
        //     points = parseInt(localStorage.getItem("points"));
        // }
        // if(currentOrder.length>3){
        //     points+=currentOrder.length*20;
        // }

        // localStorage.setItem("points",points);
    }
}

function loadFavo(){
    if (!(localStorage.getItem("fav") === null)) {
        currentOrder=JSON.parse(localStorage.getItem("fav"));
        viewCOrder();
    }
}

function viewPoints(){
    if (localStorage.getItem("points") === null) {
        alert("No ponts, place an order to get points");
    }else{
        alert("Your points : "+localStorage.getItem("points"));
    }
}

function placeOrder(){
    if(overallOrder.length>0){

        var points=0;
        if (!(localStorage.getItem("points") === null)) {
            points = parseInt(localStorage.getItem("points"));
        }
        if(overallOrder.length>3){
            points+=overallOrder.length*20;
        }

        localStorage.setItem("points",points);

        overallOrder=[];
        viewOVrder();
        alert("Thank you for the order !");
    }else{
        alert("Order is empty !");
    }
}

function donate(){
    alert("Thank you for your donation !");
}